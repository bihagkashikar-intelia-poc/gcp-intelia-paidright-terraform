variable "pdrght_project" {}
variable "pdrght_fldr_prefix" {
    type = string
    default = "pr"
}

variable "pdrght_fldr_name" {
    type = string
    default = "team-de"
}

variable "pdrght_env" {}

variable "pdrght_org_id" {
    type = string
    default = "685404422673"
}
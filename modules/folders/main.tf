resource "google_folder" "pdrght_fldrs" {
  display_name = "${var.pdrght_fldr_prefix}"-"${var.pdrght_fldr_name}"-"${var.pdrght_env}"
  parent       = "organizations/${var.pdrght_org_id}"
}
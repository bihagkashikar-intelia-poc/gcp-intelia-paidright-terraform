terraform {
  backend "gcs" {
    bucket = "paidrightintelia-tfstate"
    prefix = "env/dev"
  }
}
locals {
  env = "dev"
}

provider "google" {
  project = "${var.project}"
}

module "pdrght_devfolder" {
  source = "../../modules/folders"
  project = "${var.pdrght_project}"
  env = "${local.dev}"
}